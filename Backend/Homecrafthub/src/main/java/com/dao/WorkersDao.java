
package com.dao;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.model.Workers;
import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.rest.api.v2010.account.Message;

@Service
public class WorkersDao {

	@Autowired
	WorkersRepository workersRepository;

	@Autowired
	private JavaMailSender mailSender;

	private static final int OTP_LENGTH = 6;

	private static final String ACCOUNT_SID = "AC2f4bcb960b99e9126a5337740afc3525";
	private static final String AUTH_TOKEN = "a41f7e4d07d6bc4a5ee9c7c2329e46b4";
	private static final String TWILIO_PHONE_NUMBER = "+16592177740";

	static {
		Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	}

	public List<Workers> getAllWorkers() {
		return workersRepository.findAll();
	}

	public Workers workersLogin(String emailId, String password) {

		Workers workers = workersRepository.findByEmailId(emailId);

		// Check if employee exists and the provided password matches the stored
		// password
		if (workers != null) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			if (passwordEncoder.matches(password, workers.getPassword())) {
				// Passwords match, return the employee
				return workers;
			}
		}
		return null;
	}
	
	public List<Workers> getWorkersByPincode(String pincode) {
		return workersRepository.findByPincode(pincode);
	}

	public Workers addWorker(Workers workers) {

		String otp = generateOTP();

		workers.setOtp(otp);

		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		String encryptedPwd = bcrypt.encode(workers.getPassword());
		workers.setPassword(encryptedPwd);

		Workers savedWorkers = workersRepository.save(workers);

		sendOtpViaSms(savedWorkers);
		sendWelcomeEmail(savedWorkers);

		return savedWorkers;

	}

	private void sendWelcomeEmail(Workers workers) {

		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(workers.getEmailId());
		message.setSubject("Welcome to our website");
		message.setText("Dear " + workers.getWorkerName() + ",\n\n" + "Thank you for registration " + workers.getOtp());

		mailSender.send(message);
	}

	private String generateOTP() {
		Random random = new Random();
		StringBuilder otp = new StringBuilder();

		for (int i = 0; i < OTP_LENGTH; i++) {
			otp.append(random.nextInt(10));
		}

		return otp.toString();
	}

	private void sendOtpEmail(Workers workers) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(workers.getEmailId());
		message.setSubject("Password Reset OTP");
		message.setText(
				"Dear " + workers.getWorkerName() + ",\n\n" + "Your OTP for password reset is: " + workers.getOtp());

		mailSender.send(message);
	}

	private void sendOtpViaSms(Workers workers) {
		try {
			Message message = Message.creator(new com.twilio.type.PhoneNumber(workers.getPhoneNumber()),
					new com.twilio.type.PhoneNumber(TWILIO_PHONE_NUMBER),
					"Your OTP for registration is: " + workers.getOtp()).create();

			System.out.println("OTP sent successfully via SMS.");
		} catch (ApiException e) {
			if (e.getCode() == 21614) {
				// Twilio error code 21614 corresponds to "Trial accounts cannot
				// send messages to unverified numbers"
				System.err.println("OTP not sent: Twilio trial accounts cannot send messages to unverified numbers.");
			} else {
				System.err.println("Error sending OTP via SMS: " + e.getMessage());
			}
		}
	}
	

	public Workers getWorkerEmailId(String emailId) {
		Workers workers = workersRepository.findByEmailId(emailId);
		if (workers != null) {
			String otp = generateOTP();
			workers.setOtp(otp);
			sendOtpEmail(workers);
		}
		return workers;
	}

	public Workers updateWorker(Workers workers) {
		return workersRepository.save(workers);
	}


	public void deleteWorkerById(int workerId) {
		workersRepository.deleteById(workerId);

	}

	public Workers updateWorkerPassword(String emailId, String password) {
		Workers worker = workersRepository.findByEmailId(emailId);
		if(worker != null){
			
			BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
			String encryptedPwd = bcrypt.encode(password);
			worker.setPassword(encryptedPwd);
			workersRepository.save(worker);
		}
		return worker;
	}

	public Workers fetchWorkerOtp(String otp) {
		Workers workers = workersRepository.findByOtp(otp);
			return workers;
	}
	

	public Workers address(String emailId, String address) {
		
		Workers worker = workersRepository.findByEmailId(emailId);
		
			SimpleMailMessage message = new SimpleMailMessage();
			message.setTo(emailId);
			message.setSubject("Welcome to our website");
			message.setText("Dear " + emailId+ ",\n\n" + "Thank you for registration " + address);

			mailSender.send(message);
			
		return worker;
	}
}

