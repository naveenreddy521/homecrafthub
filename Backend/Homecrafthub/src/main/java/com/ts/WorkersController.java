package com.ts;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.WorkersDao;
import com.model.Address;
import com.model.Credentials;
import com.model.Workers;

@CrossOrigin(origins="http://localhost:4200")
@RestController
public class WorkersController {
	
	@Autowired
	WorkersDao workersDao;
	
	@GetMapping("getAllWorkers")
	public List<Workers> getAllWorkers(){
		return workersDao.getAllWorkers();
	}
	
	@PostMapping("addWorker")
	public Workers addWorker(@RequestBody Workers workers) {
		return workersDao.addWorker(workers);
	}
	
	@GetMapping("workersLogin/{emailId}/{password}")
	public Workers workerLogin(@PathVariable String emailId, @PathVariable String password) {
		return workersDao.workersLogin(emailId, password);
	}
	
	@GetMapping("getWorkersByPincode/{pincode}")
	public List<Workers> getWorkersByPincode(@PathVariable("pincode") String pincode) {
	    return workersDao.getWorkersByPincode(pincode);
	}
	
	@PutMapping("updateWorkers")
	public Workers updateWorker(@RequestBody Workers workers) {
		return workersDao.updateWorker(workers);
	}
	
	@DeleteMapping("deleteWorkersById/{workerId}")
	public String deleteWorkerById(@PathVariable int workerId) {
		workersDao.deleteWorkerById(workerId);
		return "Employee with empId:" + workerId + " Deleted Successfully!!!";
	}
	
	@GetMapping("getWorkerEmailId/{emailId}")
	public Workers getWorkerEmailId(@PathVariable String emailId) {
		return workersDao.getWorkerEmailId(emailId);
	}
	
	@PutMapping("updateWorkerPassword")
	public Workers updateWorkerPassword(@RequestBody Credentials cred) {
		System.out.println(cred.getWorkerEmailId());
		System.out.println(cred.getWorkerPassword());
		
		return workersDao.updateWorkerPassword(cred.getWorkerEmailId(),cred.getWorkerPassword());
	}
	
	@GetMapping("fetchWorkerOtp/{otp}")
	public Workers fetchWorkerOtp(@PathVariable String otp){
		return workersDao.fetchWorkerOtp(otp);
	}
	
	@PostMapping("address")
	public Workers address(@RequestBody Address address){
		
		System.out.println(address.getAddress());
		System.out.println(address.getEmailId());
		
		return workersDao.address(address.getEmailId(),address.getAddress());
		
	}
	
}