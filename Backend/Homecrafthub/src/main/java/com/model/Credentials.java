package com.model;

public class Credentials {
	
	private String workerEmailId;
	private String workerPassword;
	private String customerEmailId;
	private String CustomerPassword;
	
	public Credentials() {
		
	}

	public String getWorkerEmailId() {
		return workerEmailId;
	}

	public void setWorkerEmailId(String workerEmailId) {
		this.workerEmailId = workerEmailId;
	}

	public String getWorkerPassword() {
		return workerPassword;
	}

	public void setWorkerPassword(String workerPassword) {
		this.workerPassword = workerPassword;
	}

	public String getCustomerEmailId() {
		return customerEmailId;
	}

	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}

	public String getCustomerPassword() {
		return CustomerPassword;
	}

	public void setCustomerPassword(String customerPassword) {
		CustomerPassword = customerPassword;
	}
}

