import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrl: './forgotpassword.component.css'
})
export class ForgotpasswordComponent implements OnInit{

  emailId: string = '';
  workers:any;


  constructor(private service: ServicesService,private toastr: ToastrService,private router: Router) {
    this.workers= {
      emailId: '',
      password: ''
    };
  }

  ngOnInit() {
    // Subscribe to changes in the DataService
    this.service.currentEmailId.subscribe((emailId: string) => {
      this.emailId = emailId;
    });
  }

  passwordChanged(regForm: any) {
      this.workers.emailId = regForm.emailId;
      this.workers.password = regForm.password;

      console.log(  this.workers.emailId);
      console.log(  this.workers.password);

      this.service.updateWorkerPassword(this.workers).subscribe((data: any) => { console.log(data); });
       this.toastr.success('password changed');
      this.router.navigate(['login']);
    }





}
