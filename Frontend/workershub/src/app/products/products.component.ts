import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrl: './products.component.css'
})
export class ProductsComponent implements OnInit {

  products: any;
  emailId: any;
  selectedProduct : any;
  cartProducts : any;


  constructor(private service : ServicesService) {
    this.emailId = localStorage.getItem('emailId');
    this.cartProducts = [];
    this.products = [
      {

        name: '18w philips',
        description: 'Philips Tube Lights: Brighten your space with energy-efficient brilliance',
        price: 450,
        imageUrl: './assets/images/18w philips.jpg'
      },
      {

        name: 'Acc cement',
        description: 'ACC Cement: Trusted strength for enduring structures. Order yours now! ',
        price: 500,
        imageUrl: './assets/images/Acc cement.png'
      },
      {

        name: 'Bricks ',
        description: 'High-quality bricks: Build your dreams with strength and reliability.',
        price: 25,
        imageUrl: './assets/images/brick_image.png'
      },
      {

        name: 'UPVC Pipes',
        description: 'Upvc Water Pipes: Durable, Reliable, Pure Flow for Your Home!',
        price: 100,
        imageUrl: './assets/images/UPVC pipes.jpg'
      },
      {

        name: 'Radha tmt 12mm',
        description: 'Radha TMT Rods: The Strength Your Structure Deserves. Buy Now!',
        price: 40000,
        imageUrl: './assets/images/sugna_tmt_image_3.jpg'
      },
      {

        name: 'Wipro LED Bulb',
        description: 'Illuminate your world with Wipro LED bulbs: energy-efficient brilliance!',
        price: 260,
        imageUrl: './assets/images/wipro.jpg'
      },
      {

        name: 'Teak Wood Door',
        description: 'Elegant, durable teak doors for timeless home sophistication.',
        price: 5000,
        imageUrl: './assets/images/teak-wood-door-frame-500x500.jpg'
      },
      {

        name: 'Asian Paints',
        description: '"Transform your space with vibrant hues from Asian Paints today!"',
        price: 1260,
        imageUrl: './assets/images/Asian Paints.jpg'
      },
      {

        name: 'Vizaq tmt rod',
        description: '"Vizaq TMT Rods: The Strength Your Structure Deserves."',
        price: 1560,
        imageUrl: './assets/images/vizag_4 tmt.png'
      },
      {

        name: 'Pillar Tap',
        description: 'Steel Water Taps: Durable, Reliable, Pure Flow for Your Home!',
        price: 600,
        imageUrl: './assets/images/pillar tap.jpg'
      },
      {

        name: 'Haveels Red Wire',
        description: 'Havells Red Wire: Reliable, durable, safety ensured electrical solutions',
        price: 400,
        imageUrl: './assets/images/havells__red wire.jpg'
      },
      {

        name: 'Ambuja Cement',
        description: 'Ambuja Cement: Building your dreams with strength and reliability',
        price: 500,
        imageUrl: './assets/images/ambuja_ppc.jpg'
      },



    ]
  }

  ngOnInit() {
  }
  addToCart(product: any) {
    this.service.addToCart(product);
  }

}
