import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrl: './workers.component.css'
})
export class WorkersComponent implements OnInit {
  
  workers: any;
  customerId: any;

  //Dependency Injection for EmpService
  constructor(private service:ServicesService) {
    this.customerId = localStorage.getItem('customerId');
  }

  ngOnInit() {
    this.service.getAllWorkers().subscribe((data: any) => {
      console.log(data);
      this.workers = data;
    });
  }
}
