
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  isUserLoggedIn: boolean;
  loginStatus: any;
  cartItems: any;

  private emailIdSource = new BehaviorSubject<string>('');
  currentEmailId = this.emailIdSource.asObservable();

  //Dependency Injection for HttpClient
  constructor(private http: HttpClient) {
    this.cartItems = [];
    this.isUserLoggedIn = false;
    this.loginStatus = new Subject();
  }

  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }
  getIsUserLogged(): boolean {
    return this.isUserLoggedIn;
  }
  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }
  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }

  //--------AddToCart-------------------------

  addToCart(product: any) {
    this.cartItems.push(product);
    console.log(this.cartItems.Size);
  }
  getCartItems() {
    return this.cartItems;
  }
  setCartItems() {
    this.cartItems.splice();
  }

  //---passingEmailId-----------------------------

  passEmailId(emailId: string) {
    this.emailIdSource.next(emailId);
  }


  getAllCountries() {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  //----Customers------------------------------  

  getAllCustomers(): any {
    return this.http.get('http://localhost:8086/getAllCustomers');
  }

  customerLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8086/customerLogin/' + emailId + '/' + password).toPromise();
  }

  addCustomer(customer: any): any {
    return this.http.post('http://localhost:8086/addCustomer', customer);
  }

  deleteCustomerById(customerId: any): any {
    return this.http.delete('http://localhost:8086/deleteCustomerById' + customerId);
  }

  getCustomerotp(emailId: any): any {
    return this.http.get('http://localhost:8086/getCustomerEmailId' + emailId);
  }

  getUpdateCustomerPassword(customer: any) {
    return this.http.put('http://localhost:8086/updateCustomerPassword', customer);
  }

  getCustomerRegistrationOtp(otp: any): any {
    return this.http.get('http://localhost:8086/customerFetchOtp/' + otp);
  }

  //-----Workers---------------------  


  getAllWorkers() {
    return this.http.get('http://localhost:8086/getAllWorkers');
  }

  addWorkers(worker: any): any {
    return this.http.post('http://localhost:8086/addWorker', worker);
  }

  workersLogin(emailId: any, password: any): any {
    return this.http.get('http://localhost:8086/workersLogin/' + emailId + '/' + password);
  }

  updateWorkers(worker: any) {
    return this.http.get('http://localhost:8086/updateWorkers', worker);
  }

  getWorkersByPincode(pincode: any): any {
    return this.http.get('http://localhost:8086/getWorkerByPincode/' + pincode);
  }

  getWorkerRegistrationOtp(otp: any): any {
    return this.http.get('http://localhost:8086/workerFetchOtp/' + otp).toPromise();
  }

  updateWorkerPassword(workers: any): any {
    return this.http.put('http://localhost:8086/updateWorkerPassword', workers);
  }

  getWorkerOtp(emailId: any): any {
    return this.http.get('http://localhost:8086/getWorkerEmailId/' + emailId);
  }

}