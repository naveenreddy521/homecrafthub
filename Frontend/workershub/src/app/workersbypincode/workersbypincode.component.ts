import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-workersbypincode',
  templateUrl: './workersbypincode.component.html',
  styleUrl: './workersbypincode.component.css'
})
export class WorkersbypincodeComponent implements OnInit {

  pincode: any;
  workers: any[] | undefined;  
  formModel: any = {};

  // Dependency Injection for EmpService
  constructor(private service: ServicesService) {
  }

  ngOnInit() {
  }

  getWorkers() {
    this.service.getWorkersByPincode(this.formModel.pincode).subscribe((data: any) => {
      this.workers = Array.isArray(data) ? data : [data];  // Ensure workers is an array
      console.log(this.workers);
    });
  }
}