import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otpverification',
  templateUrl: './otpverification.component.html',
  styleUrl: './otpverification.component.css'
})
export class OtpverificationComponent implements OnInit {
 
  otp1: any;  
  formModel: any = {};

  // Dependency Injection for EmpService
  constructor(private service: ServicesService,private toastr: ToastrService,private router: Router) {
  }

  ngOnInit() {
  }

  getOtp() {
    this.service.getWorkerOtp(this.formModel.emailId).subscribe(
      (data: any) => {
        this.otp1 = data.otp;  
        console.log(this.otp1);
      },
      (error: any) => {
        console.error('Error fetching workers:', error);
      }
    );

    this.service.passEmailId(this.formModel.emailId);
  }

  loginSubmit(loginForm: any) {
    if (loginForm.otp2 == this.otp1) {
      console.log("hii");
     this.router.navigate(['forgotpassword']);
    }
    else{
      this.toastr.error("Invalid Credentials");
    }
  }


}
