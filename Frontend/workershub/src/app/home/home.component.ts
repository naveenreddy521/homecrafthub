import { Component } from '@angular/core';
import { ServicesService } from '../services.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
  
  products:any[];
  
  constructor(private service: ServicesService) {
    this.products = [
      { name: '18w philips', description: 'Philips Tube Lights: Brighten your space with energy-efficient brilliance', price: 450, imageUrl: './assets/images/18w philips.jpg' },
      { name: 'Acc cement', description: 'ACC Cement: Trusted strength for enduring structures. Order yours now! ', price: 500, imageUrl: './assets/images/Acc cement.png' },
      { name: 'Bricks ', description: 'High-quality bricks: Build your dreams with strength and reliability.', price: 25, imageUrl: './assets/images/brick_image.png' },
      { name: 'UPVC Pipes', description: 'Upvc Water Pipes: Durable, Reliable, Pure Flow for Your Home!', price: 100, imageUrl: './assets/images/UPVC pipes.jpg' },
      { name: 'Radha tmt 12mm', description: 'Radha TMT Rods: The Strength Your Structure Deserves. Buy Now!', price: 40000, imageUrl: './assets/images/sugna_tmt_image_3.jpg' },
      { name: 'Wipro LED Bulb', description: 'Illuminate your world with Wipro LED bulbs: energy-efficient brilliance!', price: 260, imageUrl: './assets/images/wipro.jpg' },
      { name: 'Teak Wood Door', description: 'Elegant, durable teak doors for timeless home sophistication.', price: 5000, imageUrl: './assets/images/teak-wood-door-frame-500x500.jpg' },
      { name: 'Asian Paints', description: '"Transform your space with vibrant hues from Asian Paints today!"', price: 1260, imageUrl: './assets/images/Asian Paints.jpg' }
    ]
  }

}
