import { Component } from '@angular/core';
import { ServicesService } from '../services.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-workersregister',
  templateUrl: './workersregister.component.html',
  styleUrl: './workersregister.component.css'
})
export class WorkersregisterComponent {
  loginStatus: any;
  countries: any;
  customer: any;
  confirmPassword: string = ''; // Added variable for confirmPassword

  constructor(private service: ServicesService, private router: Router,private toastr:ToastrService) {
    this.customer = {
      customerName: '',
      emailId: '',
      password: '',
      phoneNumber: '',
      pincode: ''
    };
  }

  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }
  registerSubmit(regForm: any) {
    if (this.customer.password == this.confirmPassword) {

      this.customer.customerName = regForm.customerName;
      this.customer.pincode = regForm.pincode;
      this.customer.emailId = regForm.emailId;
      this.customer.password = regForm.password;
      this.customer.phoneNumber = regForm.phoneNumber;

      console.log(this.customer);
      console.log(this.customer.pincode);

      this.service.addWorkers(this.customer).subscribe((data: any) => { console.log(data); });
       this.toastr.success('Registration success');
      this.router.navigate(['login']);
    }
    else {
      console.log('Password and Confirm Password must be the same.');
      this.toastr.error('Registration Failed');
    }
  }
}







