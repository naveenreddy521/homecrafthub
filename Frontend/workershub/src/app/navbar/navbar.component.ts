import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.css'
})
export class NavbarComponent implements OnInit {
  loginStatus: any;
  cartItems: any;

  constructor(private service: ServicesService,private router:Router) {
    this.cartItems = service.getCartItems();
  }
  // loginStatus: any;

  // constructor(private service: ServicesService,private router: Router) {
  // }

  ngOnInit() {
    this.service.getLoginStatus().subscribe((data: any) => {
      this.loginStatus = data;
    });
  }
  navigateToconstruction() : void{
    this.router.navigateByUrl('/construction');
  }
  
  navigateToelectrical():void{
    this.router.navigateByUrl('/electrical');
  }
  
  navigateTofixtures():void{
    this.router.navigateByUrl('/fixtures');
  }
  

  navigateTocustomerLogin(): void {
    this.router.navigateByUrl('/login'); // Assuming 'login' is the route path for your login component
  }

  navigateTotrainerLogin(): void{
    this.router.navigateByUrl('/workerlogin');
  }

  }



