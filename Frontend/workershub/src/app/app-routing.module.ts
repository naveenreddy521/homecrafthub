import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ProductsComponent } from './products/products.component';
import { LogoutComponent } from './logout/logout.component';
import { authGuard } from './auth.guard';
import { WorkersComponent } from './workers/workers.component';
import { NavbarComponent } from './navbar/navbar.component';
import { WorkersbypincodeComponent } from './workersbypincode/workersbypincode.component';
import { HomeComponent } from './home/home.component';
import { CartComponent } from './cart/cart.component';
import { OtpverificationComponent } from './otpverification/otpverification.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { WorkersloginComponent } from './workerslogin/workerslogin.component';
import { WorkersregisterComponent } from './workersregister/workersregister.component';
import { WorkerregistrationotpComponent } from './workerregistrationotp/workerregistrationotp.component';
import { ConstructionComponent } from './construction/construction.component';
import { ElectricalComponent } from './electrical/electrical.component';
import { FixturesComponent } from './fixtures/fixtures.component';

const routes: Routes = [
  {path:'',component:HomeComponent},
  {path:'login',      component:LoginComponent},
  {path:'workerlogin',component:WorkersloginComponent},
  {path:'register',   component:RegisterComponent},
  {path:'workersregister',component:WorkersregisterComponent},
  {path:'aboutus',    component:AboutusComponent},
  {path:'products',      component:ProductsComponent},
  {path:'construction',   component:ConstructionComponent},
  {path:'electrical',   component:ElectricalComponent},
  {path:'fixtures',    component:FixturesComponent},
  {path:'logout',     component:LogoutComponent},
  {path:'otpverification',  component:OtpverificationComponent},
  {path:'forgotpassword', component:ForgotpasswordComponent},
  {path:'workers',     canActivate:[authGuard],     component:WorkersComponent},
  {path:'workersbypincode', canActivate:[authGuard],  component:WorkersbypincodeComponent},
  {path:'cart', component:CartComponent},
  {path:'workerregistrationotp', component:WorkerregistrationotpComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
